import { switchMap } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SignoService } from './../../_service/signo.service';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { Signo } from './../../_model/signo';
import { MatTableDataSource } from '@angular/material/table';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-signo',
  templateUrl: './signo.component.html',
  styleUrls: ['./signo.component.css']
})
export class SignoComponent implements OnInit {

  cantidad: number = 0;
  displayedColumns = ['idSigno', 'paciente', 'fecha', 'temperatura', 'pulso', 'ritmoRespiratorio', 'acciones'];
  dataSource: MatTableDataSource<Signo>;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private signoService: SignoService, 
    private snackBar: MatSnackBar, 
    public route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.signoService.signoCambio.subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.sortingDataAccessor = (item, property) => {
        switch(property) {
          case 'paciente': return item.paciente.nombres + ' ' + item.paciente.apellidos;
          default: return item[property];
        }
      };
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.dataSource.filterPredicate = (data, filter) => {
        const dataStr = data.idSigno + (data.paciente.nombres.toLowerCase() + ' ' + data.paciente.apellidos.toLowerCase()) + data.fecha + data.temperatura + data.pulso + data.ritmoRespiratorio;
        return dataStr.indexOf(filter) != -1; 
      }
    });

    this.signoService.mensajeCambio.subscribe(data => {
      this.snackBar.open(data, 'Aviso', {
        duration: 2000,
      });
    });

    this.signoService.listarPageable(0, 10).subscribe(data => {
      console.log(data);
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);
      this.dataSource.sortingDataAccessor = (item, property) => {
        switch(property) {
          case 'paciente': return item.paciente.nombres;
          default: return item[property];
        }
      };
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.dataSource.filterPredicate = (data, filter) => {
        const dataStr = data.idSigno + (data.paciente.nombres.toLowerCase() + ' ' + data.paciente.apellidos.toLowerCase()) + data.fecha + data.temperatura + data.pulso + data.ritmoRespiratorio;
        return dataStr.indexOf(filter) != -1; 
      }
    });
  }

  eliminar(idSigno: number) {
    this.signoService.eliminar(idSigno).pipe(switchMap(() => {
      return this.signoService.listar();
    })).subscribe(data => {
      this.signoService.signoCambio.next(data);
      this.signoService.mensajeCambio.next('Se eliminó');
    });
  }

  filtrar(valor: string) {
    this.dataSource.filter = valor.trim().toLowerCase();
    //this.dataSource.filter = valor;
  }

  mostrarMas(e: any) {
    this.signoService.listarPageable(e.pageIndex, e.pageSize).subscribe(data => {
      console.log(data);
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);
      this.dataSource.sortingDataAccessor = (item, property) => {
        switch(property) {
          case 'paciente': return item.paciente.nombres;
          default: return item[property];
        }
      };
      this.dataSource.sort = this.sort;
      this.dataSource.filterPredicate = (data, filter) => {
        const dataStr = data.idSigno + (data.paciente.nombres.toLowerCase() + ' ' + data.paciente.apellidos.toLowerCase()) + data.fecha + data.temperatura + data.pulso + data.ritmoRespiratorio;
        return dataStr.indexOf(filter) != -1; 
      }
    });
  }

}
