import { MatSnackBar } from '@angular/material/snack-bar';
import { PacienteEdicionComponent } from './../../paciente/paciente-edicion/paciente-edicion.component';
import { MatDialog } from '@angular/material/dialog';
import { Signo } from './../../../_model/signo';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { SignoService } from './../../../_service/signo.service';
import { map, switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { PacienteService } from './../../../_service/paciente.service';
import { Paciente } from './../../../_model/paciente';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-signo-edicion',
  templateUrl: './signo-edicion.component.html',
  styleUrls: ['./signo-edicion.component.css']
})
export class SignoEdicionComponent implements OnInit {

  form: FormGroup;

  id: number;
  edicion: boolean;
  signo: Signo;
  pacientes: Paciente[] = [];
  maxFecha: Date = new Date();
  pacienteSeleccionado: Paciente;
  temperatura: string;
  pulso: string;
  ritmoRespiratorio: string;

  myControlPaciente: FormControl = new FormControl();
  pacientesFiltrados: Observable<any[]>;

  constructor(private pacienteService: PacienteService,
    private signoService: SignoService,
    private dialog : MatDialog,
    private route: ActivatedRoute, private router: Router,
    private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.signo = new Signo();

    this.form = new FormGroup({
      'paciente': this.myControlPaciente,
      'fecha': new FormControl(new Date()),
      'temperatura': new FormControl(),
      'pulso': new FormControl(),
      'ritmoRespiratorio': new FormControl()
    });

    this.listarPacientes();
    this.pacientesFiltrados = this.myControlPaciente.valueChanges.pipe(map(val => this.filtrarPacientes(val)));

    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = params['id'] != null;
      this.initForm();
    });

    //Suscribiendo a los eventos emitidos desde el dialogo
    this.pacienteService.mensajeCambio.subscribe(data => {
      this.snackBar.open(data, 'AVISO', {
        duration: 2000
      });
    });

    this.pacienteService.pacienteCambio.subscribe(data => {
      this.pacientes = data;
    });

    this.pacienteService.pacienteSeleccionadoDialog.subscribe(data => {
      console.log('Paciente registrado y a seleccionar');
      console.log(data);
      this.pacienteSeleccionado = data;
      this.myControlPaciente.setValue(this.pacienteSeleccionado);
    });
  }

  initForm() {
    if (this.edicion) {
      this.signoService.listarPorId(this.id).subscribe(data => {
        this.signo.idSigno = data.idSigno;
        this.pacienteSeleccionado = data.paciente;
        let fecha = data.fecha;
        let temperatura = data.temperatura;
        let pulso = data.pulso;
        let ritmoRespiratorio = data.ritmoRespiratorio;
        this.myControlPaciente.setValue(this.pacienteSeleccionado);
        this.form = new FormGroup({
          'paciente': this.myControlPaciente,
          'fecha': new FormControl(fecha),
          'temperatura': new FormControl(temperatura),
          'pulso': new FormControl(pulso),
          'ritmoRespiratorio': new FormControl(ritmoRespiratorio)
        });
      });
    }
  }

  get f() { return this.form.controls; }

  filtrarPacientes(val : any){    
    if (val != null && val.idPaciente > 0) {
      return this.pacientes.filter(option =>
        option.nombres.toLowerCase().includes(val.nombres.toLowerCase()) || option.apellidos.toLowerCase().includes(val.apellidos.toLowerCase()) || option.dni.includes(val.dni));
    } else {
      return this.pacientes.filter(option =>
        option.nombres.toLowerCase().includes(val.toLowerCase()) || option.apellidos.toLowerCase().includes(val.toLowerCase()) || option.dni.includes(val));
    }
  }

  seleccionarPaciente(e: any) {
    this.pacienteSeleccionado = e.option.value;
  }

  mostrarPaciente(val : Paciente){
    return val ? `${val.nombres} ${val.apellidos}` : val;
  }

  listarPacientes() {
    this.pacienteService.listar().subscribe(data => {
      this.pacientes = data;
    });
  }

  operar() {
    if(this.form.invalid){
      return;
    }
    this.signo.paciente = this.pacienteSeleccionado;
    let fechaObject = new Date(this.form.value['fecha']);
    this.signo.fecha = moment(fechaObject).format('YYYY-MM-DDTHH:mm:ss');
    this.signo.temperatura = this.form.value['temperatura'];
    this.signo.pulso = this.form.value['pulso'];
    this.signo.ritmoRespiratorio = this.form.value['ritmoRespiratorio'];

    console.log(this.signo);
    
    if (this.signo != null && this.signo.idSigno > 0) {
      this.signoService.modificar(this.signo).pipe(switchMap(() => {
        return this.signoService.listar();
      })).subscribe(data => {
        this.signoService.signoCambio.next(data);
        this.signoService.mensajeCambio.next("Se modificó");
      });
    } else {
      this.signoService.registrar(this.signo).pipe(switchMap(() => {
        return this.signoService.listar();
      })).subscribe(data => {
        this.signoService.signoCambio.next(data);
        this.signoService.mensajeCambio.next("Se registró");
      });
    }
    this.router.navigate(['signo-vital']);
  }

  abrirDialogo() {
    this.dialog.open(PacienteEdicionComponent, {
      width: '300px',
      data: true
    });
  }

}
