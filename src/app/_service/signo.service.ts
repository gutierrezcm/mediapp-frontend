import { Subject } from 'rxjs';
import { Signo } from './../_model/signo';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SignoService {

  signoCambio = new Subject<Signo[]>();
  mensajeCambio = new Subject<string>();

  url: string = `${environment.HOST}/signos`;

  constructor(private http: HttpClient) { }

  listarPageable(page: number, size: number) {
    return this.http.get<any>(`${this.url}/pageable?page=${page}&size=${size}`);
  }

  listarPorId(idSigno: number) {
    return this.http.get<Signo>(`${this.url}/${idSigno}`);
  }

  registrar(signo: Signo) {
    return this.http.post(this.url, signo);
  }

  modificar(signo: Signo) {
    return this.http.put(this.url, signo);
  }

  eliminar(idSigno: number) {
    return this.http.delete(`${this.url}/${idSigno}`);
  }

  listar() {
    return this.http.get<Signo[]>(this.url);
  }

}
